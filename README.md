# OpenML dataset: adult

https://www.openml.org/d/45068

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Prediction task is to determine whether a person makes over 50K a year. Extraction was done by Barry Becker from the 1994 Census database. A set of reasonably clean records was extracted using the following conditions: ((AAGE>16) && (AGI>100) && (AFNLWGT>1)&& (HRSWK>0))

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45068) of an [OpenML dataset](https://www.openml.org/d/45068). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45068/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45068/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45068/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

